package pl.sdacademy

import spock.lang.Specification

class BookSearchQuerySpec extends Specification {

    def "Should return list of books of a given author"() {
        given:
        TestLibrary testLibrary = new TestLibrary()

        when:
        def result = new BookSearchQuery("Tolkien").execute(testLibrary)

        then:
        result.size() == 2
        result.containsAll(aBook, anotherBook)

        where:
        aBook = new Book("Tolkien", "Lord of The Rings")
        anotherBook = new Book("Tolkien", "Silmarilion")
    }

    class TestLibrary implements Library {

        List<Book> books = new ArrayList<>()

        TestLibrary() {
            books.add(new Book("Tolkien", "Lord of The Rings"))
            books.add(new Book("Tolkien", "Silmarilion"))
            books.add(new Book("Thiel", "From Zero to One"))
        }

        @Override
        List<Book> searchFor(String author) {
            books.findAll {book -> book.getAuthor() == author}
        }
    }
}
