package pl.sdacademy;

import java.util.List;

public interface Library {

    public List<Book> searchFor(String author);
}
