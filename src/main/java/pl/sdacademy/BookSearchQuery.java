package pl.sdacademy;

import java.util.List;

class BookSearchQuery {
    private final String author;

    public BookSearchQuery(String author) {
        this.author = author;
    }

    public List<Book> execute(Library library) {
        return library.searchFor(author);
    }
}