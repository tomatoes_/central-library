W repozytorium znajdziesz kod, który tworzy nową instancję (olbrzymiej) biblioteki głównej (CentralLibraryCatalogue)
dla każdego zapytania. Zmień powyższy kod tak aby używał wzorca Singleton dla CentralLibrary, żeby upewnić się, że tylko
jedna instancja bibiloteki jest tworzona.

Kiedy to zrobisz pomyśl o zależnościach i powiązaniach pomiędzy obiektami (pojęcie coupling). Być może chcielibyśmy użyć
BookSearchQuery w inny sposób tak, aby działał na katalogu biblioteki uniwersyteckiej (UniversityLibraryCatalogue).
Zrefaktoryzuj kod korzystając z zasady dependency inversion aby na to pozwolić. Chcemy przełamać zależność na konkretnej
klasie i stworzyć bardziej elastyczny, reużywalny kod.

Napisz teraz test jednostkowy dla BookSearchQuery używając jakiejś TestLibrary, która zawiera tylko niewielki zbiór
znanych książek.

UWAGA: implementacja searchFor() jest oczywiście nieprawdziwa. Nie przejmuj się tym - w ramach zadania ta prosta
implementacja będzie wystarczająca.